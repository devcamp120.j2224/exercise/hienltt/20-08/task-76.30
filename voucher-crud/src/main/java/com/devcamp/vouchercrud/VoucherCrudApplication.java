package com.devcamp.vouchercrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VoucherCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(VoucherCrudApplication.class, args);
	}

}
