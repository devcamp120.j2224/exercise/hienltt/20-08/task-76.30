package com.devcamp.vouchercrud.repository;

import java.util.List;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import com.devcamp.vouchercrud.model.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {
    
    @Query(value = "SELECT * FROM vouchers ORDER BY phan_tram_giam_gia DESC", nativeQuery = true)
	List<CVoucher> getCVoucherDESC();
	
    @Transactional
    @Modifying
    @Query(value = "UPDATE vouchers SET phan_tram_giam_gia = :phanTram WHERE ma_voucher  = :ma_voucher", nativeQuery = true)
    int updatePhanTram(@Param("ma_voucher") String maVoucher, @Param("phanTram") String phanTram);

}
